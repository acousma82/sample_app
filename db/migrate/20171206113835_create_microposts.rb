class CreateMicroposts < ActiveRecord::Migration[5.1]
  def change
    create_table :microposts do |t|
      t.text :content
      t.references :user, foreign_key: true #The foreign key reference is a database-level constraint indicating that the user id in the microposts table refers to the id column in the users table.

      t.timestamps
    end
    add_index :microposts, [:user_id, :created_at]
  end
end

# Because we expect to retrieve all the microposts associated with a given user id in reverse order of creation, we add an index on the user_id and created_at columns:

#add_index :microposts, [:user_id, :created_at]

#By including both the user_id and created_at columns as an array, we arrange for Rails to create a multiple key index, which means that Active Record uses both keys at the same time.

#With the migration in Listing 13.3, we can update the database as usual: