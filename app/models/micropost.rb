class Micropost < ApplicationRecord
  #tells the micropost model that a micropost belongs to a user, more specific to a user_id
  #the user id inside the micropost model is hence called a foreign key. We don't need to specify it because the user model corresponds to a user id.
  belongs_to :user
   
  
  # the -> is  lambda function which takes in a bloc and returns a proc
  default_scope -> { order(created_at: :desc) } # default scope is used to set the default order in which elements are retrieved from the database. Here it is called with the order argument specifying the the elements by which the microposts should be ordered. The :desc attribute is used to make sure thta it ordered descending from the newest to the oldest micropost.
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 140 }
  validate  :picture_size

  private

    # Validates the size of an uploaded picture.
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end

end
