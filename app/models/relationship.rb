class Relationship < ApplicationRecord

#every relationships consist of a follower and and a followed. this code connects the follower and followed id to the corresponding user id inside the relationship model
belongs_to :follower, class_name: "User" 
belongs_to :followed, class_name: "User"
#presence validation. these are not really necessary anymore since rails 5 was introduced. It is left in for completeness
validates :follower_id, presence: true 
validates :followed_id, presence: true

    
end
