class User < ApplicationRecord
  # the option dependent: :destroy arranges for the dependent microposts to be destroyed when the user itself is destroyed
  has_many :microposts, dependent: :destroy
  #the user class(model) has many active relationships since there is no class active relationships we have to specify the classname of active relationships which is relationship.rb. Here the foreign key is follower_id. Because the follower doesn't correspond to a model like in the case of :user we have to tell rails explicitly the foreign key . Inside the active relationships table it identifies whom the user is following.
  has_many :active_relationships, class_name: "Relationship",
                                 foreign_key: "follower_id",
                                 dependent: :destroy
  has_many :passive_relationships, class_name:  "Relationship",
                                   foreign_key: "followed_id",
                                   dependent:   :destroy
  #making following out of followeds with :following source: followed. a user now has an array like collection made of users whom he is following. It can be accessed via user.following. For more clarification see table.                            
  has_many :following, through: :active_relationships, source: :followed

  #creates an array like collection made of users who are following the current user. It can be accessed via user.followers. For more clarification see table.
  has_many :followers, through: :passive_relationships, source: :follower
  # die virtuellen Attribute remember_token, activation_token und reset_token werden initialisiert und mit schreib und lesemethoden ausgerüstet. Sie sind virtuell weil sie nicht in der Tabelle existiert und nur im Cookie gespeichert werden bzw. im account_activaton_link und im password reset link. Dadurch könenn sie für ihre je spezifischen Funktionen mit den entsprechenden digests in der Datenbank abgeglichen werden.
  attr_accessor :remember_token, :activation_token, :reset_token
  before_save   :downcase_email
  before_create :create_activation_digest
   
  validates :name, presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
#    Adds methods to set and authenticate against a BCrypt password. This mechanism requires you to have a       password_digest attribute.
  has_secure_password #fügt die authenticate Methode für den user hinzu so das man mit Hilfe von user.authenticate("password") herausfinden kann ob das eingegebene Passwort stimmt. fügt das virtuelle attribute password hinzu. 
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true # allow nil tue weil has secure password shaut ob der benutzer ein passwort hat das nicht leer ist und damit man beim updaten eines Users das Passwortfeld leer lässt.

# Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :    # je nach entwicklungsumgebung wird viel oder wenig cpu leistung zur Berechnung des hashes verwendet
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

# Returns a random token.
  def User.new_token
    SecureRandom.urlsafe_base64 #ein zufälliger base64 string mit 22 digits wird erzeugt
  end

# Remembers a user in the database for use in persistent sessions.
  def remember
    self.remember_token = User.new_token # der new_token wird als remember_token mit dem user als attribut verknüpft, welches allerdings nicht als atribut in der user tabelle existiert und deshalb ein virtuelles Attribut ist.
    update_attribute(:remember_digest, User.digest(remember_token)) # remember_digest wird kreiert indem der Token durch User.digest verdaut wird und ein hash digest entsteht der an :remember_digest übergeben wird 
  end

  # Returns true if the given token matches the digest.
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil? # when digest nil ist soll die methode false zurückgeben damit Bcrypt keinen Fehler hervorrufen kann, wenn man sich bei zwei BRowsern eingeloggt hat und sich nur bei einem abmeldet, aber beide schließt.
    digest = send("#{attribute}_digest")
    BCrypt::Password.new(digest).is_password?(token) #takes a token, hashes it and compares it to the original hash_digest from the database. If hash_digest of given token is equal to the one saved in the database, authenticatedß returns true.
  end


  def forget
    update_attribute(:remember_digest, nil)
  end

  # Activates an account.
  #def activate
   # update_attribute(:activated,    true)
   # update_attribute(:activated_at, Time.zone.now)
 # end

  def activate
    update_columns(activated: true, activated_at: Time.zone.now)
  end

  # Sends activation email.
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

# Sets the password reset attributes.
  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest,  User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  # Sends password reset email.
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  # Returns true if a password reset has expired.
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

  # Returns a user's status feed.
  def feed
    #In rails User.first.following.map(&:id) === User.first.following_ids
    #old sql query that doesnt scale well: 
    #micropost.where("user_id IN (?) OR user_id = ?", following_ids, id)

    # final feed implementation scales well because there is no need for an array. An SQL Subselect is used instead.
    following_ids = "SELECT followed_id FROM relationships
                     WHERE  follower_id = :user_id"
    Micropost.where("user_id IN (#{following_ids})
                     OR user_id = :user_id", user_id: id)
  end

  # Follows a user.
  def follow(other_user)
    following << other_user
  end

  # Unfollows a user.
  def unfollow(other_user)
    following.delete(other_user)
  end

  # Returns true if the current user is following the other user.
  def following?(other_user)
    following.include?(other_user)
  end

  
private

    # Converts email to all lower-case.Exclamation mark after the downcase method means that the actual email is downcased rather than a copy of email which is downcased instead when there is no exclamation mark. self is not necessary because ruby automatically assumes that  self is the receiver when no method receiver is specified.
    def downcase_email
      email.downcase! 
    end

    # Creates and assigns the activation token and digest.
    def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
    end

end
