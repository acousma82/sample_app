class UsersController < ApplicationController
#before filter
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy, :following, :followers] 

  before_action :correct_user,   only: [:edit, :update]
  before_action :admin_user,     only: [:destroy]


#User.paginate(available via the will-paginate gem) pulls the users out of the database one chunk at a time (30 by default), based on the :page parameter. So, for example, page 1 is users 1–30, page 2 is users 31–60, etc. If page is nil, paginate simply returns the first page.Here the page parameter comes from params[:page], which is generated automatically by will_paginate inside the view.
  def index
    @users = User.where(activated: true).paginate(page: params[:page]) 
  end

#action for showing a user in his path users/1 . Renders show.html.erb
  def show
    @user = User.find(params[:id])
    redirect_to root_url and return unless @user.activated?
    @microposts = @user.microposts.paginate(page: params[:page])
  end

#action for rendering the  signup path (/signup or users/new)
  def new
   @user = User.new
   @replace_form_path = true
  end

  def create
    
    @user = User.new(user_params)   
    if @user.save
      @user.send_activation_email
      flash[:info] = "Please check your email to activate your account."
      redirect_to root_url
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user

    else
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end
  # corresponds to the following route, when following is clicked all the users who are followed by the shown user are rendered on the folowing page.
  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end
# corresponds to the followers route, when Followers is clicked all the users who are following the user shown  are rendered on the followers page
  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end


  private

# Strong Parameters: Strong parameters sind in RoR5 obligat, sie machen die Anwendung sicher davor das böse Nutzer alle Attribute die ein User haben kann                                                               setzten können. Params ist die ROR Systemvariable in der jeder HTTP Request als "Hash of Hashes" gespeichert wird. require(:user) bedeutet das der [:user] key als attribut benötigt wird (denn hier sind alle Userdaten die ins Formular eingegeben wurden als values gespeichert), anonsten wird eine Fehlermeldung erzeugt. permit() bedeutet das alle [:user] attribute(alle keys des user hashes) die als Symbole in der Klammer übergeben werden Schreibzugriff haben, also mit Nutzerdaten über das Formular beschrieben werden dürfen.
      def user_params
          params.require(:user).permit(:name, :email, :password, :password_confirmation) # This code returns a version of the params hash with only the permitted attributes (while raising an error if the :user attribute is missing). 
                                   
      end
    
    # Before filters

    
# Confirms the correct (logged-in) user
      def correct_user
        @user = User.find(params[:id])
        redirect_to(root_url) unless current_user?(@user)
      end

      def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
    


end
