require 'test_helper'

class MicropostsInterfaceTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test "micropost interface" do
    #login as michael
    log_in_as(@user) 
    # get the hompepage
    get root_path 
    # make sure pagination is invoked for the homepage
    assert_select 'div.pagination'
    assert_select 'input[type=file]' 
    # Invalid submission with no content
    assert_no_difference 'Micropost.count' do
      post microposts_path, params: { micropost: { content: "" } }
    end
    assert_select 'div#error_explanation'
    # Valid submission
    content = "This micropost really ties the room together"
    picture = fixture_file_upload('test/fixtures/rails.png', 'image/png')
    assert_difference 'Micropost.count', 1 do
      post microposts_path, params: { micropost: { content: content, picture: picture } }
    end
    #assert that there is a picture assigned to the micropost instance
    assert assigns(:micropost).picture?
    #assert that we are redirected where we came from
    assert_redirected_to root_url
    #actually visit the the redirected page
    follow_redirect!
    #assert that the new content shows on the hoomepage
    assert_match content, response.body
    # Delete post, first make sure that there is a delete link
    assert_select 'a', text: 'delete'
    first_micropost = @user.microposts.paginate(page: 1).first
    # the Micropost.count should be one Micropost less after we issued a sucesful micropost delete request
    assert_difference 'Micropost.count', -1 do
      delete micropost_path(first_micropost)
    end
    # Visit different user (no delete links)
    get user_path(users(:archer))
    assert_select 'a', text: 'delete', count: 0
  end

test "micropost sidebar count" do
    log_in_as(@user)
    get root_path
    assert_match "#{@user.microposts.count} microposts", response.body
    # User with zero microposts
    other_user = users(:malory)
    log_in_as(other_user)
    get root_path
    assert_match "0 microposts", response.body
    other_user.microposts.create!(content: "A micropost")
    get root_path
    assert_match "#{other_user.microposts.count} micropost", response.body
  end
 
end